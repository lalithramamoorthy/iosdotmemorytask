//
//  RadioAnswers.swift
//  DotMemoryTask
//
//  Created by LEDlab on 11/15/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit
import DLRadioButton

protocol UIViewControllerDelegate:class {
    func next(value: [Int])
}

class RadioAnswers: UIView {
    var delegate:UIViewControllerDelegate?
    let overallStackView: UIStackView = UIStackView(arrangedSubviews: [])
    var scrollView: UIScrollView = UIScrollView()
    var qvc: QuestionnaireController? = nil
    var options: [Option] = []
    var button = UIButton()
    var value: [Int] = []
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    required init(opts: Array<Option>) {
        super.init(frame: .zero)
        setup(opts: opts)
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initView(isCheck: Bool) {
        let buttonStackView = createRadioColumn(checked: isCheck)
        let nextButton = createNextButton()
        var spacing: CGFloat = 20
        var top: CGFloat = 0
        if isCheck {
            top = 10
            spacing = 10
        }
        let overallStackView = UIStackView(arrangedSubviews: [buttonStackView, nextButton])
        overallStackView.translatesAutoresizingMaskIntoConstraints = false
        nextButton.widthAnchor.constraint(equalToConstant: 10).isActive = true
        overallStackView.layoutMargins = UIEdgeInsets(top: top, left: 5, bottom: 0, right: 5)
        overallStackView.isLayoutMarginsRelativeArrangement = true
        overallStackView.axis = .vertical
        overallStackView.distribution = .fillProportionally
        overallStackView.alignment = .fill
        overallStackView.spacing = spacing
        
        scrollView = UIScrollView(frame: UIScreen.main.bounds)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        scrollView.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        self.scrollView.addSubview(overallStackView)
        self.addSubview(self.scrollView)
        self.scrollView.isScrollEnabled = false
        
        scrollView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[innerView]|",
                                           options: NSLayoutFormatOptions(rawValue:0),
                                           metrics: nil,
                                           views: ["innerView":overallStackView]))
        
        scrollView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[innerView]|",
                                           options: NSLayoutFormatOptions(rawValue:0),
                                           metrics: nil,
                                           views: ["innerView":overallStackView]))
        
        scrollView.addConstraint(
            NSLayoutConstraint(item: scrollView,
                               attribute: .width,
                               relatedBy: .equal,
                               toItem: overallStackView,
                               attribute: .width,
                               multiplier: 1.0,
                               constant: 0))
        scrollView.isUserInteractionEnabled = true
    }
    
    private func createRadioColumn(checked: Bool) -> UIStackView {
        var buttonArrangedSubviews: [UIView] = []
        var imageRequired = false
        if options[0].image != nil {
            imageRequired = true
        }
        
        let frame = CGRect(x: 10, y: 10, width: 300, height: 35);
        let radioGroup = createRadioButton(frame: frame, title: options[0].option!, value: options[0].value!,  imageRequired: imageRequired);
        buttonArrangedSubviews.append(radioGroup)
        radioGroup.isIconSquare = checked;
        var otherButtons : [DLRadioButton] = [];
        for i in stride(from: 1, to: options.count, by: 1) {
            let frame = CGRect(x: 10, y: 20, width: 300, height: 35);
            let radioButton = createRadioButton(frame: frame, title: options[i].option!, value: options[i].value!, imageRequired: imageRequired);
            radioButton.isIconSquare = checked;
            buttonArrangedSubviews.append(radioButton)
            otherButtons.append(radioButton);
        }
        radioGroup.otherButtons = otherButtons;
        if checked {
            radioGroup.isMultipleSelectionEnabled = true
        }
        
        // set selection state programmatically
        let buttonStackView = UIStackView(arrangedSubviews: buttonArrangedSubviews)
        buttonStackView.axis = .vertical
        buttonStackView.distribution = .fillProportionally
        buttonStackView.alignment = .fill
        buttonStackView.spacing = 5
        buttonStackView.isUserInteractionEnabled = true
        return buttonStackView
    }
    
    private func createNextButton() -> UIButton {
        button = UIButton()
        button.setTitle("Next", for: .normal)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addTarget(self, action:#selector(self.nextButton), for: UIControlEvents.touchUpInside)
        button.isEnabled = false
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        return button
    }
    
    private func createRadioButton(frame : CGRect, title : String, value : String, imageRequired: Bool) -> DLRadioButton {
        let radioButton = DLRadioButton();
        radioButton.titleLabel!.font = UIFont.systemFont(ofSize: 18);
        var choice = NSMutableAttributedString(string: title)
        if imageRequired {
            choice = getUpdatedTitle(title: title)
        }
        radioButton.tag = Int(value)!
        radioButton.setAttributedTitle(choice, for: [])
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        radioButton.titleLabel?.numberOfLines = 0
        radioButton.iconColor = UIColor.black;
        radioButton.indicatorColor = UIColor.orange;
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
        radioButton.addTarget(self, action: #selector(self.logSelectedButton), for: UIControlEvents.touchUpInside);
        return radioButton;
    }
    
    private func getUpdatedTitle(title: String) -> NSMutableAttributedString {
        
        let choice = NSMutableAttributedString(string: " ")
        let imageAttachment = textAttachment(fontSize: 14)
        let imageString = NSAttributedString(attachment: imageAttachment)
        let titleString = NSMutableAttributedString(string: title)
        let spaceString = NSMutableAttributedString(string: " ")
        choice.append(imageString)
        choice.append(spaceString)
        choice.append(titleString)
        return choice
    }
    
    func textAttachment(fontSize: CGFloat) -> NSTextAttachment {
        let font = UIFont.systemFont(ofSize: fontSize) //set accordingly to your font, you might pass it in the function
        let textAttachment = NSTextAttachment()
        let image = UIImage(named: "very_deactivated")!
        textAttachment.image = image
        let mid = font.descender + font.capHeight
        let integral: CGRect = CGRect(x: 0, y: font.descender - image.size.height / 2 + mid + 2, width: image.size.width, height: image.size.height)
        textAttachment.bounds = integral
        return textAttachment
    }
    
    private func setup(opts: Array<Option>) {
        self.options = opts
    }
    
    @objc @IBAction private func nextButton() {
        delegate?.next(value: self.value)
    }
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            if radioButton.selectedButtons().count > 0 {
                button.setTitleColor(UIColor.black, for: .normal)
                button.backgroundColor = UIColor.lightGray
                button.isEnabled = true
            } else {
                button.isEnabled = false
                button.backgroundColor = .clear
                button.setTitleColor(UIColor.darkGray, for: .normal)
            }
            value.removeAll()
            for button in radioButton.selectedButtons() {
                value.append(button.tag)
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            button.setTitleColor(UIColor.black, for: .normal)
            button.backgroundColor = UIColor.lightGray
            button.isEnabled = true
            value.removeAll()
            print("++++++++++++TAG", radioButton.tag)
            value.append(radioButton.tag)
        }
    }
}

