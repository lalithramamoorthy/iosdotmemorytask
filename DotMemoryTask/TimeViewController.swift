//
//  TimeViewController.swift
//  DotMemoryTask
//
//  Created by LEDlab on 11/16/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit

protocol ModalViewControllerDelegate:class {
    func dismissed(selectedDates: [Date])
}


class TimeViewController: UIViewController {
    var selectedTimes = [Date]()
    var weekday: Date? = nil
    var weekend: Date? = nil
    @IBOutlet weak var done: UIButton!
    var delegate:ModalViewControllerDelegate?
    let timePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        done.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func weekendPickTime(_ sender: Any) {
        timePicker.locale = Locale(identifier: "en_GB")
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(TimeViewController.startTimeWeekendChanged), for: UIControlEvents.valueChanged)
    }
    @IBAction func pickTime(_ sender: Any) {
        timePicker.locale = Locale(identifier: "en_GB")
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.frame = CGRect(x: 0.0, y: 60, width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(TimeViewController.startTimeDiveChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        sender.datePickerMode = .time
        print(sender.date)
        weekday = sender.date
        if weekday != nil && weekend != nil {
            done.isEnabled = true
        }
        print(weekday)
        
    }
    
    @objc func startTimeWeekendChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        sender.datePickerMode = .time
        print(sender.date)
        weekend = sender.date
        if weekday != nil && weekend != nil {
            done.isEnabled = true
        }
        print(weekend)
        
    }
    @IBAction func done(_ sender: Any) {
        selectedTimes.append(weekday!)
        selectedTimes.append(weekend!)
        delegate?.dismissed(selectedDates: selectedTimes)
    }
    
}
