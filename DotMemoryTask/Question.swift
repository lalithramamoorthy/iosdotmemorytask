//
//  Question.swift
//  DotMemoryTask
//
//  Created by LEDlab on 10/26/18.
//  Copyright © 2018 neu. All rights reserved.
//

import Foundation

struct Question<T : Codable>: Codable {
    
    var type: String?
    var tag: String?
    var question: String?
    var options: Array<Option>?
    var optionfields: Array<OptionField>?
    var precondition: [String : T]?
    var conditions: Array<Condition>?
    var nextquestion: Int?
    var anchortext: [String : String]?
    
}
