//
//  Condition.swift
//  DotMemoryTask
//
//  Created by LEDlab on 10/26/18.
//  Copyright © 2018 neu. All rights reserved.
//

import Foundation

struct Condition: Codable {
    var option: String?
    var value: Int?
}
