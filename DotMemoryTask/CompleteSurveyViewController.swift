//
//  CompleteSurveyViewController.swift
//  DotMemoryTask
//
//  Created by LEDlab on 12/17/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit

class CompleteSurveyViewController: UIViewController {
    var answers = [String : Any]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func finish(_ sender: Any) {
        let preferences = UserDefaults.standard
        let userInfo = preferences.dictionary(forKey: "userInfo")
        let surveyId = userInfo?["TotalSurveys"]
        
        var surveyData = [String: Any]()
        surveyData["EndTime"] = Date()
        surveyData["Responses"] = answers
        surveyData["Time"] = 0
        surveyData["StartTime"] = 0
        surveyData["Burst"] = 1
        
        DBUtils.sharedInstance.addSurvey(surveyId: surveyId! as! Int + 1, newUserResponse: surveyData)
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
        self.view.window?.rootViewController = controller
    }
    


}
