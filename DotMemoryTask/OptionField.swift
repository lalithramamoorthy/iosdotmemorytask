//
//  OptionField.swift
//  DotMemoryTask
//
//  Created by LEDlab on 10/26/18.
//  Copyright © 2018 neu. All rights reserved.
//

import Foundation

struct OptionField: Codable {
    
    var tag: String?
    var label: String?
    var options: Array<Option>?
}
