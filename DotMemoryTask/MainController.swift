//
//  MainController.swift
//  DotMemoryTask
//
//  Created by Lalith Kumar Ramamoorthy
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    
    @IBOutlet weak var attentionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "assessmentTaken") != nil {
            if preferences.bool(forKey: "assessmentTaken") {
                let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
                self.view.window?.rootViewController = controller
            }
            
        }
    }
    
    @IBAction func onCLick(_ sender: UIButton) {
        
    }
}
