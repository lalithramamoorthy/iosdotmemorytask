//
//  LoginViewController.swift
//  DotMemoryTask
//
//  Created by LEDlab on 12/1/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class LoginViewController: UIViewController {
    @IBOutlet weak var participantId: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done(_ sender: Any) {
        guard participantId.text != "" else {
            return
        }
        
        let preferences = UserDefaults.standard
        preferences.set(participantId.text!, forKey: "id")
        preferences.synchronize()
        DBUtils.sharedInstance.registerUser()
        
        if preferences.object(forKey: "assessmentTaken") != nil {
            if preferences.bool(forKey: "assessmentTaken") {
                performSegue(withIdentifier: "loginViewControllerSegue", sender: self)
            } else {
                performSegue(withIdentifier: "loginToGame", sender: self)
            }
        } else {
            performSegue(withIdentifier: "loginToGame", sender: self)
        }
    }

}
