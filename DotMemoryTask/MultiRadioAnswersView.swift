//
//  MultiAnswersView.swift
//  DotMemoryTask
//
//  Created by LEDlab on 11/14/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit
import DLRadioButton

class MultiRadioAnswersView: UIView {
    var delegate:UIViewControllerDelegate?
    let overallStackView: UIStackView = UIStackView(arrangedSubviews: [])
    let overallStackView1: UIStackView = UIStackView(arrangedSubviews: [])
    var scrollView: UIScrollView = UIScrollView()
    var options: [Option] = []
    var anchorText = [String : String]()
    var button = UIButton()
    var value: [Int] = []
    var answers = [Int : Int]()
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    required init(opts: Array<Option>) {
        super.init(frame: .zero)
        setup(opts: opts)
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initView() {
        let field1 = createRadioColumn(goal:1)
        let field2 = createRadioColumn(goal:2)
        let field3 = createRadioColumn(goal:3)
        let goals = createGoalView()
        let firstAnchor = createAnchorView(anchor: anchorText["first"]!)
        let lastAnchor = createAnchorView(anchor: anchorText["last"]!)
        let nextButton = createNextButton()
        let overallStackView = UIStackView(arrangedSubviews: [field1, field2, field3])
        overallStackView.axis = .horizontal
        overallStackView.distribution = .fillEqually
        overallStackView.alignment = .center
        overallStackView.spacing = 1
        overallStackView.translatesAutoresizingMaskIntoConstraints = false
        
        let overallStackView1 = UIStackView(arrangedSubviews: [goals, firstAnchor, overallStackView, lastAnchor, nextButton])
        overallStackView1.layoutMargins = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        overallStackView1.isLayoutMarginsRelativeArrangement = true
        overallStackView1.axis = .vertical
        overallStackView1.distribution = .fillProportionally
        overallStackView1.alignment = .fill
        overallStackView1.spacing = 1
        overallStackView1.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView = UIScrollView(frame: UIScreen.main.bounds)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        scrollView.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue)))
        self.scrollView.addSubview(overallStackView1)
        self.addSubview(self.scrollView)
        self.scrollView.isScrollEnabled = false
        scrollView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[innerView]|",
                                           options: NSLayoutFormatOptions(rawValue:0),
                                           metrics: nil,
                                           views: ["innerView":overallStackView1]))
        
        scrollView.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[innerView]|",
                                           options: NSLayoutFormatOptions(rawValue:0),
                                           metrics: nil,
                                           views: ["innerView":overallStackView1]))
        
        scrollView.addConstraint(
            NSLayoutConstraint(item: scrollView,
                               attribute: .width,
                               relatedBy: .equal,
                               toItem: overallStackView1,
                               attribute: .width,
                               multiplier: 1.0,
                               constant: 0))
        scrollView.isUserInteractionEnabled = true
    }
    
    private func createNextButton() -> UIButton {
        button = UIButton()
        button.setTitle("Next", for: .normal)
         button.setTitleColor(UIColor.black, for: .normal)
        button.frame.size = CGSize(width: 40.0, height: 20.0)
        button.addTarget(self, action:#selector(self.nextButton), for: UIControlEvents.touchUpInside)
        button.isEnabled = false
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        return button
    }

    private func createGoalView() -> UIStackView {
        let goal1 = UILabel()
        goal1.text = "GOAL 1"
        goal1.font = UIFont.systemFont(ofSize: 18);
        goal1.numberOfLines = 0;
        goal1.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let goal2 = UILabel()
        goal2.text = "GOAL 2"
        goal2.font = UIFont.systemFont(ofSize: 18);
        goal2.numberOfLines = 0;
        goal2.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let goal3 = UILabel()
        goal3.text = "GOAL 3"
        goal3.font = UIFont.systemFont(ofSize: 18);
        goal3.numberOfLines = 0;
        goal3.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let goalStackView = UIStackView(arrangedSubviews: [goal1, goal2, goal3])
        goalStackView.isLayoutMarginsRelativeArrangement = true
        goalStackView.axis = .horizontal
        goalStackView.distribution = .fill
        goalStackView.alignment = .fill
        goalStackView.spacing = 0
        goalStackView.translatesAutoresizingMaskIntoConstraints = false
        return goalStackView
    }
    
    private func createAnchorView(anchor: String) -> UIStackView {
        let leftArrow = UILabel()
        leftArrow.text = "<--"
        leftArrow.font = UIFont.systemFont(ofSize: 18);
        leftArrow.numberOfLines = 0;
        leftArrow.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let rightArrow = UILabel()
        rightArrow.text = "-->"
        rightArrow.font = UIFont.systemFont(ofSize: 18);
        rightArrow.numberOfLines = 0;
        rightArrow.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let anchorLabel = UILabel()
        anchorLabel.text = anchor
        anchorLabel.font = UIFont.systemFont(ofSize: 18);
        anchorLabel.numberOfLines = 0;
        anchorLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let anchorStackView = UIStackView(arrangedSubviews: [leftArrow, anchorLabel, rightArrow])
        anchorStackView.isLayoutMarginsRelativeArrangement = true
        anchorStackView.axis = .horizontal
        anchorStackView.distribution = .fill
        anchorStackView.alignment = .fill
        anchorStackView.spacing = 0
        anchorStackView.translatesAutoresizingMaskIntoConstraints = false
        return anchorStackView
    }
    
    private func createRadioColumn(goal: Int) -> UIStackView {
        var buttonArrangedSubviews: [UIView] = []
        let imageRequired = false
        let frame = CGRect(x: 10, y: 10, width: 300, height: 35);
        let radioGroup = createRadioButton(frame: frame, title: options[0].option!, value: options[0].value!, imageRequired: imageRequired, goal: goal);
        
        buttonArrangedSubviews.append(radioGroup)
        radioGroup.isIconSquare = false;
        var otherButtons : [DLRadioButton] = [];
        
        for i in stride(from: 1, to: options.count, by: 1) {
            let frame = CGRect(x: 10, y: 20, width: 300, height: 35);
            let radioButton = createRadioButton(frame: frame, title: options[i].option!, value: options[i].value!, imageRequired: imageRequired, goal: goal);
            buttonArrangedSubviews.append(radioButton)
            otherButtons.append(radioButton);
        }
        
        radioGroup.otherButtons = otherButtons;
        // set selection state programmatically
        let buttonStackView = UIStackView(arrangedSubviews: buttonArrangedSubviews)
        buttonStackView.isLayoutMarginsRelativeArrangement = true
        buttonStackView.axis = .vertical
        buttonStackView.distribution = .fill
        buttonStackView.alignment = .fill
        buttonStackView.spacing = 2
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.isUserInteractionEnabled = true
        return buttonStackView
    }
    
    private func createRadioButton(frame : CGRect, title : String, value: String, imageRequired: Bool, goal: Int) -> DLRadioButton {
        let radioButton = DLRadioButton();
        radioButton.titleLabel!.font = UIFont.systemFont(ofSize: 18);
        var choice = NSMutableAttributedString(string: title)
        if imageRequired {
            choice = getUpdatedTitle(title: title)
        }
        radioButton.tag = goal
        radioButton.setAttributedTitle(choice, for: [])
        radioButton.iconColor = UIColor.black;
        radioButton.indicatorColor = UIColor.orange;
//        radioButton.setValue(goal, forKey: "Goal")
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
        radioButton.addTarget(self, action: #selector(self.logSelectedButton), for: UIControlEvents.touchUpInside);
        self.scrollView.addSubview(radioButton);
        
        return radioButton;
    }
    
    private func getUpdatedTitle(title: String) -> NSMutableAttributedString {
        
        let choice = NSMutableAttributedString(string: " ")
        let imageAttachment = textAttachment(fontSize: 14)
        let imageString = NSAttributedString(attachment: imageAttachment)
        let titleString = NSMutableAttributedString(string: title)
        let spaceString = NSMutableAttributedString(string: " ")
        choice.append(imageString)
        choice.append(spaceString)
        choice.append(titleString)
        return choice
    }
    
    func textAttachment(fontSize: CGFloat) -> NSTextAttachment {
        let font = UIFont.systemFont(ofSize: fontSize) //set accordingly to your font, you might pass it in the function
        let textAttachment = NSTextAttachment()
        let image = UIImage(named: "very_deactivated")!
        textAttachment.image = image
        let mid = font.descender + font.capHeight
        let integral: CGRect = CGRect(x: 0, y: font.descender - image.size.height / 2 + mid + 2, width: image.size.width, height: image.size.height)
        textAttachment.bounds = integral
        return textAttachment
    }
    
    private func setup(opts: Array<Option>) {
        self.options = opts
    }
    
    @objc @IBAction private func nextButton() {
        for (_, val) in answers.sorted(by: { $0.0 < $1.0 }) {
            value.append(val)
        }
        delegate?.next(value: value)
    }
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        let group: Int = radioButton.tag
        answers[group] = Int(radioButton.titleLabel!.text!)
        if answers.count == 3 {
            button.setTitleColor(UIColor.black, for: .normal)
            button.isEnabled = true
            button.backgroundColor = .lightGray
        }
        print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
        
    }
}
