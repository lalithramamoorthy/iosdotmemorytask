//
//  DBUtils.swift
//  DotMemoryTask
//
//  Created by LEDlab on 12/2/18.
//  Copyright © 2018 neu. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class DBUtils {
    
    static let sharedInstance = DBUtils()
    
    private init() {
        
    }
    
    public func getFirebaseInstance() -> Firestore {
        return Firestore.firestore()
    }
    
    public func registerUser() {
        let preferences = UserDefaults.standard
        let key = "id"
        if preferences.object(forKey: key) != nil {
            let id = preferences.string(forKey: key)
        }
        
        let db = self.getFirebaseInstance()
        let docRef = db.collection("UsersIos").document(preferences.string(forKey: key)!)
        var userInfo = [String: Any]()
        userInfo["Burst"] = 0
        userInfo["TotalSurveys"] = 0
        userInfo["StartDate"] = Date()
        userInfo["Calibration"] = 0
        var goals = [String: Any]()
        goals["Goal1"] = ""
        goals["Goal2"] = ""
        goals["Goal3"] = ""
        userInfo["Goals"] = goals
        docRef.setData( userInfo, completion: { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                preferences.set(userInfo, forKey: "userInfo")
                preferences.synchronize()
                print("Document successfully written!")
            }
        })
    }
    
    public func addSurvey(surveyId: Int,  newUserResponse: [String : Any]) {
        
        let preferences = UserDefaults.standard
        let pid = preferences.string(forKey: "id")!
        let db = self.getFirebaseInstance()
        
        var userInfo = [String: Any]()
        userInfo["TotalSurveys"] = surveyId
        userInfo["Burst"] = 1
        var uinfo = preferences.dictionary(forKey: "userInfo")
        uinfo!["TotalSurveys"] = surveyId
        preferences.set(uinfo, forKey: "userInfo")
        db.collection("UsersIos").document(pid).setData(userInfo, merge: true)
        db.collection("UsersIos")
            .document(pid)
            .collection("Surveys").document(String(surveyId)).setData(newUserResponse)
    }
    
    public func addAssessmentCalibration(score : [String: Any]) {
        
        let preferences = UserDefaults.standard
        let pid = preferences.string(forKey: "id")!
        let db = self.getFirebaseInstance()
        
        var uinfo = preferences.dictionary(forKey: "userInfo")
        uinfo!["Calibration"] = score
        
        preferences.set(uinfo, forKey: "userInfo")
        db.collection("UsersIos").document(pid).setData(uinfo!, merge: true)

    }
}
