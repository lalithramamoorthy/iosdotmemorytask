//
//  ConfigurationViewController.swift
//  DotMemoryTask
//
//  Created by LEDlab on 10/9/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
   
    @IBOutlet weak var beginButton: UIButton!
    @IBOutlet weak var practiceTrialsField: UITextField!

    @IBOutlet weak var setSizeField: UITextField!
    
    var setSize: Int = 1
    var practiceTrials: Int = 1
    var pickOption = ["1","2","3","4","5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let pickerView = UIPickerView()
        pickerView.delegate = self
        setSizeField.inputView = pickerView

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        setSizeField.text = pickOption[row]
    }

    @IBAction func begin(_ sender: Any) {
        setSize = Int(setSizeField.text!)!
        practiceTrials = Int(practiceTrialsField.text!)!
        
        performSegue(withIdentifier: "configurationSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let navVC = segue.destination as! UINavigationController
        
        let destVC = navVC.viewControllers.first as! ViewController
        
        destVC.dotsTobeShown = setSize
        destVC.practice = practiceTrials

        
    }

}
