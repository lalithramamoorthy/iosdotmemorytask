//
//  ViewController.swift
//  DotMemoryTask
//
//  Created by Ledlab
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var ClickView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var grainView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var esmNextButton: UIButton!
    @IBOutlet weak var surveyButton: UIButton!
    
    var firebaseDotsCorrect : Array<Int> = Array()
    var firebaseDotsEuclideanDistance : Array<Double> = Array()
    var firebaseTrialDots : Array<Int> = Array()
    var firebaseTrialEuclideanScores: Array<Double> = Array()
    var firebaseTrialSizes : Array<Int> = Array()
    var firebaseMeanDotsCorrect40 : Int = 0
    
    var dotList : Array<Int> = Array()
    var pressedDotList : Array<Int> = Array()
    var correctDots: Array<Int> = Array()
    var wrongDots: Array<Int> = Array()
    var buttonMap = [Int : Bool]()
    var shouldPress = false
    var validated: Bool = false
    var speed: Double = 1.0
    var dotSpeed: Array<Double> = Array()
    var setSizes: Array<Int> = Array()
    
    var practice: Int = 0
    var timer = Timer()
    var progresstimer = Timer()
    var updateTimer = Timer()
    var counter: Int = 1
    var totalTrialPackageCount = 20 //to be 20
    var trials: Int = 2
    var dotsTobeShown: Int = 3
    var numberOfDotsShowed: Int = 0
    var numberOfDotsPressed: Int = 0
    var score: Int = 0
    
    var MAX_TIME: Float = 5.0
    var currentTime: Float = 0.0
    /**
        ESM
    **/
    var answers = [String : Any]()
    var esm = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dotSpeed.append(2)
        dotSpeed.append(1.75)
        dotSpeed.append(1.5)
        dotSpeed.append(1.25)
        dotSpeed.append(1)
        grainView?.isHidden = true
        grainView?.backgroundColor = UIColor(patternImage:  UIImage(named: "grain")!)
        labelView?.isHidden = true
        surveyButton.isHidden = true
        esmNextButton?.isHidden = true
        
       
        for case let button as UIButton in self.view.subviews {
            buttonMap[button.tag] = false
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap(_:)))
        tap.delegate = self
        ClickView?.addGestureRecognizer(tap)
        
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        ClickView?.isHidden = true
        print ("CLICK VIEW NEW TRIAL")
        newTrial()
    }
    
    
    func disableButtons()
    {
        for tagvalue in 1...36
        {
            let btnTemp = self.view.viewWithTag(tagvalue) as! UIButton;
            btnTemp.isUserInteractionEnabled = false;
        }
    }
    
    func enableButtons()
    {
        for tagvalue in 1...36
        {
            let btnTemp = self.view.viewWithTag(tagvalue) as! UIButton;
            btnTemp.isUserInteractionEnabled = true;
        }
    }
    
    func newTrial() {
        if dotsTobeShown < 5 {
            speed = dotSpeed[dotsTobeShown - 1]
        } else if dotsTobeShown >= 5 {
            speed = dotSpeed[dotSpeed.count - 1]
        }
        if practice == 0 {
            trials -= 1
        }
        self.perform(#selector(self.updateBox), with: nil, afterDelay: speed)
    }
    
    @objc func updateBox() {
        
        if(numberOfDotsShowed >= dotsTobeShown) {
            print(numberOfDotsShowed, "number of dot showed")
            timer.invalidate()
            numberOfDotsShowed = 0
            shouldPress = true
            startGame()
            return
        }
        numberOfDotsShowed += 1;
        var number = 0
        while true {
            number = Int(arc4random_uniform(35) + 1)
            if (!dotList.contains(number) && !isAdjacent(number: number)){
                break
            }
        }
        let tmpButton = self.view.viewWithTag(number) as? UIButton
        
        buttonMap[number] = true
        dotList.append(number)
        tmpButton?.backgroundColor = UIColor.black
        DispatchQueue.main.asyncAfter(deadline: .now() + speed) {
            tmpButton?.backgroundColor = UIColor.white
            self.perform(#selector(self.updateBox), with: nil, afterDelay: 0.0)
        }
    }
    
    func isAdjacent(number: Int) -> Bool {
        
        if !dotList.isEmpty {
            let lastDot = dotList.last
            let difference = abs(number - lastDot!)
            if(difference == 1 || difference == 6) {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    private func euclideanDistance(start: Int, end: Int) -> Double {
        let x1: Double = Double(start / 10)
        let y1: Double = Double(start % 10)
        let x2: Double = Double(end / 10)
        let y2: Double = Double(end % 10)
        let xDiff: Double = (x2 - x1)
        let yDiff: Double = (y2 - y1)
    
        return sqrt(xDiff * xDiff + yDiff * yDiff)
    }
    
    func showAnswers() {
        if dotList.count == 0 {
            firebaseTrialDots.append(correctDots.count)
            score = score + correctDots.count
            correctDots.removeAll()
            wrongDots.removeAll()
            pressedDotList.removeAll()
            dotList.removeAll()
            
            if (practice != 0) {
                practice = practice - 1
                if practice == 0 {
                    labelView?.isHidden = false
                    label.textAlignment = NSTextAlignment.center;
                    label?.text = "Assessment Begin"
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.labelView?.isHidden = true
                    }
                }
            }else if (practice == 0 && trials == 0) {
                counter = counter + 1
                setSizes.append(dotsTobeShown)
                firebaseTrialSizes.append(dotsTobeShown)
                firebaseTrialSizes.append(dotsTobeShown)
                print("setSizes", setSizes)
                print("recalibrating")
                trials = 2
                if score >= dotsTobeShown {
                    print("increase by 1")
                    dotsTobeShown = dotsTobeShown + 1
                } else if score < dotsTobeShown {
                    print("decrease by 1")
                    if dotsTobeShown == 1 {
                        dotsTobeShown = 1
                    } else {
                        dotsTobeShown = dotsTobeShown - 1
                    }
                }
                score = 0
                print(dotsTobeShown, "trialSet")
                if counter <= totalTrialPackageCount {
                    labelView?.isHidden = false
                    label.textAlignment = NSTextAlignment.center;
                    label?.text = "New Trial"
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.labelView?.isHidden = true
                    }
                }
            }
            
            if (counter > totalTrialPackageCount) {
                var sum: Double = 0
                for size in setSizes {
                    sum = sum + Double(size)
                }
                var average: Double = 0
                if sum > 0 && setSizes.count >= 1 {
                    average = sum/Double(setSizes.count)
                }
                
                firebaseMeanDotsCorrect40 = Int(Double(firebaseTrialDots.reduce(0, +)) / Double(firebaseTrialDots.count))
                
                
                label?.text = "score: " + String(average)
                labelView?.isHidden = false
                
                
                var overallscore = [String: Any]()
                overallscore["TrialSizes"] = firebaseTrialSizes
                
                overallscore["TrialDots"] = firebaseTrialDots
                overallscore["TrialEuclideanScores"] = firebaseTrialEuclideanScores
//                overallscore["TrialTimes"] = trialTimes.toString()
                overallscore["DotsCorrect"] = firebaseDotsCorrect
                overallscore["DotsEuclideanScores"] = firebaseDotsEuclideanDistance
//                overallscore["DotTimes"] = dotTimes.toString()
                overallscore["MeanSetSize40"] = average
//                overallscore["MeanSetSize"] = meanSetSize
                overallscore["MeanDotsCorrect40"] = firebaseMeanDotsCorrect40
                overallscore["EuclideanScore"] = firebaseTrialEuclideanScores.reduce(0, +)
//                overallscore["Time40"] = trialTimes.sum()
                
                let preferences = UserDefaults.standard
                preferences.set(overallscore, forKey: "assessmentCalibration")
                preferences.set(true, forKey: "assessmentTaken")
                preferences.synchronize()
                if !esm {
                    DBUtils.sharedInstance.addAssessmentCalibration(score: overallscore)
                    surveyButton.isHidden = false
                } else {
                    esmNextButton.isHidden = false
                    answers["esm"] = overallscore
                }
                
            } else {
                newTrial()
            }
            return
        }
        
        let dot: Int = dotList.removeFirst()
        
        if correctDots.contains(dot) {
            let image = UIImage(named: "checked") as UIImage?
            let tmpButton = self.view.viewWithTag(dot) as? UIButton
            tmpButton?.setBackgroundImage(image, for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                tmpButton?.setBackgroundImage(nil, for: .normal)
                self.showAnswers()
            }
        }
        
        if wrongDots.contains(dot) {
            let image = UIImage(named: "close") as UIImage?
            let tmpButton = self.view.viewWithTag(dot) as? UIButton
            tmpButton?.setBackgroundImage(image, for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                tmpButton?.setBackgroundImage(nil, for: .normal)
                self.showAnswers()
            }
        }
    }
    
    @objc func validateTrial() {
        var tempDotEuclideanScores: Array<Double> = Array()
        for i in 0...dotList.count - 1 {
            if (i < pressedDotList.count && dotList[i] == pressedDotList[i]) {
                correctDots.append(dotList[i])
                firebaseDotsCorrect.append(1)
                tempDotEuclideanScores.append(0)
            } else {
                var dotsPressed = 0
                if pressedDotList.count < i - 1 {
                    dotsPressed = pressedDotList[i]
                }
                tempDotEuclideanScores.append(euclideanDistance(start: dotList[i], end: dotsPressed))
                wrongDots.append(dotList[i])
                firebaseDotsCorrect.append(0)
            }
        }
        let trialEuclideanScore = tempDotEuclideanScores.safeSuffix(dotsTobeShown).reduce(0, +) / Double(dotsTobeShown)
        
        firebaseDotsEuclideanDistance.append(contentsOf: tempDotEuclideanScores)
        firebaseTrialEuclideanScores.append(trialEuclideanScore)
        showAnswers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        viewWillDisappear(false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        progresstimer.invalidate()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        DispatchQueue.main.suspend()
        self.view = nil
        dismiss(animated: false, completion: nil)
    }
    
    func startGame() {
        if shouldPress {
            self.grainView?.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.grainView?.isHidden = true
                self.enableButtons()
                self.progressView?.setProgress(self.currentTime, animated: true)
                self.perform(#selector(self.updateProgress), with: nil, afterDelay: 1.0)
            }
        }
    }
    
    @objc
    func updateProgress() {
        currentTime = currentTime + 1
        
        progressView?.progress = currentTime/MAX_TIME
        
        if currentTime <= MAX_TIME {
            perform(#selector(updateProgress), with: nil, afterDelay: 1.0)
        } else {
            currentTime = 0.0
            progressView?.progress = currentTime/MAX_TIME
            
//            if shouldPress {
                print(numberOfDotsPressed, "number of dot pressed")
                shouldPress = false
                disableButtons()
                self.numberOfDotsPressed = 0
                progresstimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: (#selector(ViewController.validateTrial)), userInfo: nil, repeats: false)
//            }
        }
    }
    
    @IBAction func buttonPressed(sender: UIButton ) {
        numberOfDotsPressed += 1;
        
        if(numberOfDotsPressed == dotsTobeShown) {
            shouldPress = false
            disableButtons()
        }

        let tmpButton = self.view.viewWithTag(sender.tag) as? UIButton
        pressedDotList.append(sender.tag)
        tmpButton?.backgroundColor = UIColor.black
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            tmpButton?.backgroundColor = UIColor.white
        }
        
        if(self.numberOfDotsPressed == self.dotsTobeShown) {
            currentTime = 999.0
        }
    }
    
    @IBAction func scheduleSurvey(_ sender: Any) {
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
        self.view.window?.rootViewController = controller
    }
    
    @IBAction func esmNext(_ sender: Any) {
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller : CompleteSurveyViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "completeSurvey") as! CompleteSurveyViewController
        controller.answers = answers
        self.view.window?.rootViewController = controller
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "scheduleSurvey" {
            
        }
    }
}

extension Collection {
    func safeSuffix(_ maxLength: Self.IndexDistance) -> Array<Element> {
        guard let index = self.index(endIndex, offsetBy: -maxLength, limitedBy: startIndex) else {
            return Array(self[startIndex ..< endIndex])
        }
        return Array(self[index ..< endIndex])
    }
}

