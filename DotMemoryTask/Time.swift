//
//  Time.swift
//  DotMemoryTask
//
//  Created by LEDlab on 11/16/18.
//  Copyright © 2018 neu. All rights reserved.
//

import Foundation
class Time {
    var hour: Int = 0
    var minute: Int = 0
    var opened: Bool = false
    init(hour: Int, minute: Int, opened: Bool) {
        self.hour = hour
        self.minute = minute
        self.opened = opened
    }
}
