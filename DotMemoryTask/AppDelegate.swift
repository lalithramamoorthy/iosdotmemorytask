//
//  AppDelegate.swift
//  DotMemoryTask
//
//  Created by Lalith Kumar Ramamoorthy on 9/18/18.
//  Copyright © 2018 neu. All rights reserved.
// ZAG20WI

import UIKit
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        UNUserNotificationCenter.current().delegate = self
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "id") != nil {
            
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            if preferences.object(forKey: "assessmentTaken") != nil {
                if preferences.bool(forKey: "assessmentTaken") {
                    let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
                    let datepreferences = UserDefaults.standard
                    if (datepreferences.object(forKey: "scheduledReminders") != nil) {
                        var reminders = datepreferences.array(forKey: "scheduledReminders")!
                        for i in stride(from: 1, to: reminders.count, by: 1) {
                            let date = reminders[i]
                            let timeInterval = Date().minutes(from: date as! Date)
                            let hourInterval = Date().hours(from: date as! Date)
                            print(timeInterval, " ", hourInterval)
                            if abs(timeInterval) < 20 {
                                controller.canTakeSurvey = true
                                reminders.remove(at: i)
                                datepreferences.set(reminders, forKey: "scheduledReminders")
                                break
                            }
                        }
                    }
                    self.window?.rootViewController = controller
                } else {
                    
                    let controller: MainController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Game") as! MainController
                    self.window?.rootViewController = controller
                }
            } else {
                
                let controller: MainController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Game") as! MainController
                self.window?.rootViewController = controller
            }
            
            
            
            
        }
        return true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        // Get the meeting ID from the original notification.
        let userInfo = response.notification.request.content.userInfo
        for (_, d) in userInfo {
            
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
            controller.date = d as! Date
            let timeInterval = Date().minutes(from: d as! Date)
            print(timeInterval)
            if abs(timeInterval) < 20 {
                controller.canTakeSurvey = true
            }
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
        // Always call the completion handler when done.
        completionHandler()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "id") != nil {
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller : ScheduleViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "Survey") as! ScheduleViewController
            let datepreferences = UserDefaults.standard
            if (datepreferences.object(forKey: "scheduledReminders") != nil) {
                var reminders = datepreferences.array(forKey: "scheduledReminders")!
                for i in stride(from: 1, to: reminders.count, by: 1) {
                    let date = reminders[i]
                    let timeInterval = Date().minutes(from: date as! Date)
                    let hourInterval = Date().hours(from: date as! Date)
                    print(timeInterval, " ", hourInterval)
                    if abs(timeInterval) < 5{
                        controller.canTakeSurvey = true
                        reminders.remove(at: i)
                        datepreferences.set(reminders, forKey: "scheduledReminders")
                        break
                    }
                }
            }
            self.window?.rootViewController = controller
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height-100, width: view.frame.width, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.numberOfLines = 0
        toastLabel.sizeToFit()
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }

