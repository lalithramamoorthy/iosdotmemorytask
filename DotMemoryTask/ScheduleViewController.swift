//
//  ScheduleViewController.swift
//  DotMemoryTask
//
//  Created by LEDlab on 11/16/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit
import UserNotifications

class ScheduleViewController: UIViewController {
    let preferences = UserDefaults.standard
    var reminder = [Date : [Time]]()
    var scheduledReminders = [Date]()
    @IBOutlet weak var surveyButton: UIButton!
    var canTakeSurvey = false
    var scheduleSurvey = true
    public var date: Date? = nil
    var isSurveyAvailable: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound];
        center.requestAuthorization(options: options) { (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
        let key = "showschedulesurvey"
        
        if preferences.object(forKey: key) == nil {
            //  Doesn't exist
        } else {
            scheduleSurvey = preferences.bool(forKey: key)
        }
        
        if scheduleSurvey {
            surveyButton.setTitle("Schedule Survey", for: .normal)
        } else {
            surveyButton.setTitle("Take Survey", for: .normal)
        }
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func surveyAction(_ sender: Any) {
        if surveyButton.title(for: .normal) == "Schedule Survey" {
            self.performSegue(withIdentifier: "time", sender: nil)
        }
        else {
            if canTakeSurvey {
                self.performSegue(withIdentifier: "question", sender: nil)
            } else {
                showToast(message: "Please take survey within 20 minutes of the scheduled notification")
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "time" {
            let controller = segue.destination as! TimeViewController
            controller.delegate = self
        }
        if segue.identifier == "question" {
            let controller = segue.destination as! QuestionnaireController
            
        }
    }
    
     func createNotification() {
        let calendar = Calendar.current
        var surveyNumber = 0
        for (date, times) in reminder {
            
            surveyNumber = 0
            for t in times {
                surveyNumber += 1
                var components = calendar.dateComponents(in: TimeZone.current, from: date)
                let year = components.year
                let month = components.month
                let day = components.day
                let hour = t.hour
                let minute = t.minute
                
                let dc = DateComponents(year: year, month: month, day: day, hour: hour, minute: minute)
                let d = calendar.date(from: dc)
                let comp = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: d!)
                scheduledReminders.append(d!)
                let timestamp = String(NSDate().timeIntervalSince1970)
                let content = UNMutableNotificationContent()
                content.title = "Attention in Every Day Life"
                content.body = "Scheduled Survey " + String(surveyNumber) + "/6"
                content.sound = UNNotificationSound.default()
                content.userInfo = [timestamp : d!] as [String : Date]
                let trigger = UNCalendarNotificationTrigger(dateMatching: comp, repeats: false)
                
                let identifier = timestamp
                let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: {
                    (error) in
                    if let error = error {
                        print (error)
                    }
                })
            }
        }
        let preferences = UserDefaults.standard
        preferences.set(scheduledReminders, forKey: "scheduledReminders")
        preferences.synchronize()
    }
}

extension ScheduleViewController: ModalViewControllerDelegate {
    
    func dismissed(selectedDates: [Date]) {
        let hour = getSelectedTime(selectedDates: [selectedDates[0]])
        
        let firstDay = Calendar.current.date(byAdding: .day, value: 1, to: selectedDates[0])!
//        if Calendar.current.isDateInWeekend(firstDay) {
//
//        }
        reminder[firstDay] = getRandomTimes(hour: hour)
        
        let secondDay = Calendar.current.date(byAdding: .day, value: 1, to: firstDay)!
        let thirdDay = Calendar.current.date(byAdding: .day, value: 1, to: secondDay)!
        let fourthDay = Calendar.current.date(byAdding: .day, value: 1, to: thirdDay)!
        let fifthDay = Calendar.current.date(byAdding: .day, value: 1, to: fourthDay)!
        reminder[secondDay] = getRandomTimes(hour: hour)
        reminder[thirdDay] = getRandomTimes(hour: hour)
        reminder[fourthDay] = getRandomTimes(hour: hour)
        reminder[fifthDay] = getRandomTimes(hour: hour)
        
        createNotification()
        surveyButton.setTitle("Take Survey", for: .normal)
        let currentLevelKey = "showschedulesurvey"
        
        let currentLevel = false
        preferences.set(currentLevel, forKey: currentLevelKey)
        
        //  Save to disk
        preferences.synchronize()
        dismiss(animated: true, completion: nil)//dismiss the presented view controller
    }
    
    func randomNumber() -> Int {
        let number = Int(arc4random_uniform(59) + 1)
        return number
    }
    
    func getRandomTimes(hour: Int) -> [Time] {
        var times = [Time]()
        times.append(Time(hour: hour, minute: randomNumber(), opened: false))
        times.append(Time(hour: hour + 1, minute: randomNumber(), opened: false))
        times.append(Time(hour: hour + 2, minute: randomNumber(), opened: false))
        times.append(Time(hour: hour + 4, minute: randomNumber(), opened: false))
        times.append(Time(hour: hour + 5, minute: randomNumber(), opened: false))
        times.append(Time(hour: hour + 7, minute: randomNumber(), opened: false))
        return times
    }
    
    func getSelectedTime(selectedDates: [Date]) -> Int {
        let selectedDate = selectedDates[0]
        let components = Calendar.current.dateComponents([.hour, .minute], from: selectedDate)
        var hour = components.hour!
        print(hour)
        return hour
    }
}

extension Date {
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        return ""
    }
}
