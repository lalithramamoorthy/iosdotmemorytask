//
//  QuestionnaireCoontroller.swift
//  DotMemoryTask
//
//  Created by LEDlab on 10/27/18.
//  Copyright © 2018 neu. All rights reserved.
//

import UIKit
import DLRadioButton
import CoreGraphics

class QuestionnaireController: UIViewController {
    
    var scrollView: UIScrollView!
    let decoder = JSONDecoder()
    var screenMinX: CGFloat = 0
    var screenMinY: CGFloat = 0
    var questions: Questionnaire<Question<Int>>? = nil
    var questionNumber: Int = 0
    var navBar: UINavigationBar = UINavigationBar()
    var answers = [String : Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenMinX = self.view.frame.minX + 10
        screenMinY = self.view.frame.minY + 50
        initScrollView()
        var jsonFile = "Survey"
        let preferences = UserDefaults.standard
        let key = "id"
        if preferences.object(forKey: key) != nil {
            let id: String = preferences.string(forKey: key)!
            if id.lowercased().starts(with: "z") {
                jsonFile = "survey_de"
            }
        }
        if let path = Bundle.main.path(forResource: jsonFile, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                questions = try decoder.decode(Questionnaire<Question<Int>>.self, from: data)
            } catch {
                //handle error
                print(error)
            }
        }
        if preferences.object(forKey: "userInfo") != nil {
            let userInfo = preferences.dictionary(forKey: "userInfo")
            let surveyId: Int = userInfo?["TotalSurveys"] as! Int
            if surveyId == 2 {
                questionNumber += 1
            }
        }
        
        loadQuestion(question: (questions?.questions?[questionNumber])!)
    }
    
    private func loadQuestion(question: Question<Int>) {
        
        let label: UILabel = displayQuestion(question: question.question!)
        label.topAnchor.constraint(equalTo: self.scrollView.topAnchor, constant: 5).isActive = true
        label.widthAnchor.constraint(equalToConstant: view.frame.width - 10).isActive = true
        let neededSize = label.sizeThatFits(CGSize(width: self.view.frame.width, height: CGFloat.greatestFiniteMagnitude))
        let height = neededSize.height
        let frame = CGRect(x: 0, y: screenMinY + height + 35, width: self.view.frame.size.width, height: self.view.frame.size.height);
        let type = question.type
        let options = question.options
        /*
         
         Radio Buttons
         
         */
        if(type == "radio") {
            let radioAnswers = RadioAnswers(frame: frame)
            radioAnswers.delegate = self as UIViewControllerDelegate
            radioAnswers.options = options!
            radioAnswers.isUserInteractionEnabled = true
            radioAnswers.initView(isCheck: false)
            self.scrollView.addSubview(radioAnswers)
            self.scrollView.updateContentView()
        }
        if(type == "checkbox") {
            let radioAnswers = RadioAnswers(frame: frame)
            radioAnswers.delegate = self as UIViewControllerDelegate
            radioAnswers.options = options!
            radioAnswers.isUserInteractionEnabled = true
            radioAnswers.initView(isCheck: true)
            self.scrollView.addSubview(radioAnswers)
            self.scrollView.updateContentView()
        }
        if(type == "instruction") {
            let button = UIButton(frame: frame)
            button.setTitle("Next", for: .normal)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.frame.size = CGSize(width: 40.0, height: 20.0)
            button.addTarget(self, action:#selector(self.nextButton), for: UIControlEvents.touchUpInside)

            self.scrollView.addSubview(button)
            self.scrollView.updateContentView()
        }
        if(type == "multiradio") {
            let anchorText = question.anchortext!
            let multiRadioAnswers = MultiRadioAnswersView(frame: frame)
            multiRadioAnswers.delegate = self as UIViewControllerDelegate
            multiRadioAnswers.options = options!
            multiRadioAnswers.anchorText = anchorText
            multiRadioAnswers.isUserInteractionEnabled = true
            multiRadioAnswers.initView()
            self.scrollView.addSubview(multiRadioAnswers)
            self.scrollView.updateContentView()
        }
        
        if(type == "game") {
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller : ViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "DMT") as! ViewController
            controller.answers = answers
            controller.esm = true
            controller.totalTrialPackageCount = 2
            controller.trials = 2
            controller.practice = 0
            self.view.window?.rootViewController = controller
        }
        view.addSubview(self.scrollView)
    }
    
    @objc func finish() {
        let preferences = UserDefaults.standard
        let userInfo = preferences.dictionary(forKey: "userInfo")
        let surveyId = userInfo?["TotalSurveys"]
        
        var surveyData = [String: Any]()
        surveyData["EndTime"] = Date()
        surveyData["Responses"] = answers
        surveyData["Time"] = 0
        surveyData["StartTime"] = 0
        surveyData["Burst"] = 1
        
        DBUtils.sharedInstance.addSurvey(surveyId: surveyId! as! Int + 1, newUserResponse: surveyData)
        
        let secondVC: UIViewController = storyboard?.instantiateViewController(withIdentifier: "Survey") as! UIViewController
        self.present(secondVC, animated:true, completion:nil)
    }
    
    public func initScrollView() {
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: screenMinY, width: self.view.frame.width, height: self.view.frame.height + 10))
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 10)
        self.scrollView.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        view.addSubview(self.scrollView)
    }
    
    private func displayQuestion(question: String) -> UILabel {
        let label = PaddingLabel(frame: CGRect(x: screenMinX + 10, y: screenMinY , width: self.view.frame.size.width, height: 20))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = question
        label.font = UIFont.systemFont(ofSize: 18);
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        label.layoutIfNeeded()
        self.scrollView.addSubview(label)
        return label
    }
    
    @objc @IBAction private func nextButton() {
        questionNumber += 1
        while let subview = scrollView.subviews.last {
            subview.removeFromSuperview()
        }
        loadQuestion(question: (questions?.questions?[questionNumber])!)
    }
}


extension UIScrollView {
    func updateContentView() {
        var lastmaxY = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY
        if lastmaxY! > UIScreen.main.bounds.maxY {
            contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
        }
        
    }
}

extension QuestionnaireController: UIViewControllerDelegate {
    func next(value: [Int]) {
        if questions?.questions?[questionNumber].type == "radio" {
            let tag = questions?.questions?[questionNumber].tag
            answers[tag!] = value[0]
        }
        if questions?.questions?[questionNumber].type == "checkbox" {
            let tag = questions?.questions?[questionNumber].tag
            answers[tag!] = value
        }
        
        if questions?.questions?[questionNumber].type == "multiradio" {
            for i in stride(from: 0, to: questions!.questions![questionNumber].optionfields!.count, by: 1) {
                let field = questions!.questions![questionNumber].optionfields![i]
                answers[field.tag!] = value[i]
            }
        }
        
        questionNumber += 1
        while let subview = scrollView.subviews.last {
            subview.removeFromSuperview()
        }
        if (questionNumber < (questions?.questions?.count)!) {
            if questions?.questions?[questionNumber].type == "multicheckbox" {
                questionNumber += 1
            }
            loadQuestion(question: (questions?.questions?[questionNumber])!)
        }
        
    }
    
    
}
